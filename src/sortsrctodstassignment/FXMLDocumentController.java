/*NOTE:
    -if destination file is NOT empty, sorted source file will be appended to
    end of destination file.
 */
package sortsrctodstassignment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Scanner;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
public class FXMLDocumentController implements Initializable {
    //GUI objects
    @FXML
    Button btSource, btDestination, btReset, btConfirm;
    @FXML
    TextField tfSource, tfDestination;
    @FXML
    //getting file, reading&writing files
    Stage theStage;//Stage required for using FileChooser
    public void setStage(Stage stage) {
        theStage = stage;
    }
    FileChooser theFileChooser = new FileChooser();
    File fileSource, fileDestination;
    List<String> sourceStrings = new ArrayList<>();
    Scanner scanner;//could've used FileReader, decided to keep it simple and just use Scanner
    FileWriter writer;
    //----------------------------------------------------------------------------------------
    @FXML
    void getSourceFileFromUserDirectory(ActionEvent event) {
        fileSource = assignFileFromUserDirectory(tfSource);
        showOrHideBTConfirm();
    }
    @FXML
    void getDestinationFileFromUserDirectory(ActionEvent event) {
        fileDestination = assignFileFromUserDirectory(tfDestination);
        showOrHideBTConfirm();
    }
    //called by above ActionEvents
    File assignFileFromUserDirectory(TextField tf) {
        File file = theFileChooser.showOpenDialog(theStage);
        //ensure valid file is passed by user
        if (file.getName().substring(file.getName().length() - 3).equals("txt")) {
            tf.setText(file.getName());
        } else {
            tf.setText("INVALID file, resubmit");
            file = null;
        }
        return file;
    }
    void showOrHideBTConfirm() {
        if(fileSource != null && fileDestination != null)
            btConfirm.setVisible(true);
        else
            btConfirm.setVisible(false);
    }
    @FXML
    void copySortedSourceToDestination(ActionEvent event) {
        try {//Please excuse the excessive use of try/catch blocks.
            //READ the file into an array of single words 
            scanner = new Scanner(fileSource);
            sourceStrings.clear();
            while (scanner.hasNext())
                sourceStrings.add(scanner.next());
            //SORT array
            Collections.sort(sourceStrings);
            //WRITE array into file
            writer = new FileWriter(fileDestination, true);
            for (int i = 0; i < sourceStrings.size(); i++) {//for every word added 
                writer.write(sourceStrings.get(i)+" ");//write new word to file followed by a space
                if ((i + 1) % 10 == 0)//move to next line for (somewhat) cleaner output
                    writer.write("\r\n");
            }
            writer.close();
            //excessive catch's :(
        } catch (FileNotFoundException ex) {
            System.out.println("FileNotFoundException, mistake made");
        } catch (IOException ex) {
            System.out.println("IOexception, mistake made");
        }
    }
    @FXML
    void resetAllVariables(ActionEvent event) {
        fileSource = null;
        fileDestination = null;
        tfSource.setText("");
        tfDestination.setText("");
        showOrHideBTConfirm();
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        btConfirm.setVisible(false);
    }
}
