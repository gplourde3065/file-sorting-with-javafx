
package sortsrctodstassignment;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class SortSrcToDstAssignment extends Application {
    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("FXMLDocument.fxml"));
        Parent root = (Parent)loader.load();
        FXMLDocumentController controller = (FXMLDocumentController)loader.getController();     
        controller.setStage(stage);
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show(); 
        stage.setTitle("Super Complicated File Sorting Program V 2.85.4.76.5.4.0.3 build 3.1 beta");
    }
    
    public static void main(String[] args) {
        launch(args);
    }
    
}
